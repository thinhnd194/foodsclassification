# Foods classification

## Requirements

- Nodejs v14 or newer

## Initialization

Clone this repo:

```
git clone https://thinhnd194@bitbucket.org/thinhnd194/foodsclassification.git
```

Navigate to workspace

```
cd foodsclassification
```

Install packages:

```
npm install
```

Build:

```
npm run build
```

Start:

```
npm start
```

Run dev server without build:

```
npm run dev
```

## Docker image

Pull the docker image

```
docker pull thinhnd194/food-classification:latest
```

Run

```
docker run -p 8000:80 thinhnd194/food-classification
```

Take it on localhost:8000

## Firebase hosting 

```
https://ml-api-d266b.web.app
```

## Netlify Webapp

```
https://vnesefoods.netlify.app
```